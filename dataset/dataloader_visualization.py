'''
Descripttion: Created by QiSen Ma
Author: QiSen Ma
Date: 2023-01-15 23:02:40
LastEditTime: 2023-02-04 13:47:01
'''
#所有数据集的数据加载类
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import numpy as np
import random
import os
from pathlib import Path
from PIL import Image
import cv2
import glob
from tqdm import tqdm
# from albumentations.pytorch import ToTensorV2

def gen_grayworld_image(image):
    r,g,b = np.split(image,3,axis=2)
 
    r_avg = np.mean(r)
    g_avg = np.mean(g)
    b_avg = np.mean(b) #可能就一个元素
    avg = (b_avg+g_avg+r_avg)/3

    r_k = avg/r_avg
    g_k = avg/g_avg
    b_k = avg/b_avg
    
    r = np.clip(r*r_k,0,255)
    g = np.clip(g*g_k,0,255)
    b = np.clip(b*b_k,0,255)
     
    image = cv2.merge([r,g,b]).astype(np.uint8)
    return image

class ISIC2017_DataLoader(Dataset):
    """ Dataloader ISIC2017

    Args:
        data_path: The path of dataset.
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='', transform=None, threshold=0.3):
        self.data_path = data_path
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        
        if "autodl-tmp" in data_path :
            self.data_list = sorted(glob.glob('/root/autodl-tmp/datasets/ISIC2017/ISIC-2017_Test_v2_Data/'  + '/*.jpg'))
            self.mask_list = sorted(glob.glob('/root/autodl-tmp/datasets/ISIC2017/ISIC-2017_Test_v2_Part1_GroundTruth/'  + '/*.png'))
        else:
            self.data_list = glob.glob(data_path+'/*.jpg')
            self.mask_list = glob.glob(data_path+'/*.png')
        
        
        print("数据集的位置", data_path)
        print('mask 数量：',len(self.mask_list))
    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file = self.data_list[index]
        mask_file = self.mask_list[index]
        # print('image_file:',image_file)
        
        #读取image
        image = cv2.imread(os.path.join(image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        image = gen_grayworld_image(image)
        
        #读取mask
        mask = cv2.imread(os.path.join(mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])
        sample['image'] = self.transform_toTensor(sample['image'])
        # sample['edge'] =    (cv2.Canny(sample['image'],30,100)/255.).astype(np.float32) #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = (sample['mask']/255.).astype(np.float32) #归一化
        
        sample['case_name'] = image_file[-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample
    

class ISIC2018_DataLoader(Dataset):
    """ Dataloader ISIC2017

    Args:
        data_path: The path of dataset.
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='', transform=None, threshold=0.3):
        self.data_path = data_path
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        
        if "autodl-tmp" in data_path :
            self.data_list = sorted(glob.glob('/root/autodl-tmp/datasets/ISIC2018/ISIC2018_Task1-2_Training_Input/'  + '/*.jpg'))
            self.mask_list = sorted(glob.glob('/root/autodl-tmp/datasets/ISIC2018/ISIC2018_Task1_Training_GroundTruth/'  + '/*.png'))
        else:
            self.data_list = glob.glob(data_path+'/*.jpg')
            self.mask_list = glob.glob(data_path+'/*.png')
        
        
        print("数据集的位置", data_path)
        print('mask 数量：',len(self.mask_list))
    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file = self.data_list[index]
        mask_file = self.mask_list[index]
        # print('image_file:',image_file)
        
        #读取image
        image = cv2.imread(os.path.join(image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        image = gen_grayworld_image(image)
        
        #读取mask
        mask = cv2.imread(os.path.join(mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])
        sample['image'] = self.transform_toTensor(sample['image'])
        # sample['edge'] =    (cv2.Canny(sample['image'],30,100)/255.).astype(np.float32) #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = (sample['mask']/255.).astype(np.float32) #归一化
        
        sample['case_name'] = image_file[-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample
    
class PH2_DataLoader(Dataset):
    """ Dataloader ISIC2017

    Args:
        data_path: The path of dataset.
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='', transform=None, threshold=0.3):
        self.data_path = data_path
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        
        if "autodl-tmp" in data_path :
            Data_list = sorted(glob.glob('/root/autodl-tmp/datasets/PH2/PH2 Dataset images/*'))
            data_suffix = '_Dermoscopic_Image'
            mask_suffix = '_lesion'
            self.data_list = []
            self.mask_list = []
            for idx in tqdm(range(len(Data_list))):
                name = Data_list[idx].split(sep='/')[-1]
                # print(Data_list[idx])
                img_path = Data_list[idx]+'/' + name + data_suffix + '/' + name + '.bmp'
                mask_path = Data_list[idx]+'/' + name + mask_suffix + '/' +name + '_lesion.bmp'
                self.data_list.append(img_path)
                self.mask_list.append(mask_path)
            # print('')
        else:
            self.mask_list = glob.glob(data_path+'/*_lesion.bmp')
            self.data_list = ['./pictures_compare/' +  item.split(sep='/')[-1][0:6] + '.bmp' for item in self.mask_list]

        
        
        print("数据集的位置", data_path)
        print('mask 数量：',len(self.mask_list))
    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file = self.data_list[index]
        mask_file = self.mask_list[index]
        # print('image_file:',image_file)
        
        #读取image
        image = cv2.imread(os.path.join(image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        image = gen_grayworld_image(image)
        
        #读取mask
        mask = cv2.imread(os.path.join(mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])
        sample['image'] = self.transform_toTensor(sample['image'])
        
        # print()
        # sample['edge'] =    (cv2.Canny(sample['image'],30,100)/255.).astype(np.float32) #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = (sample['mask']/255.).astype(np.float32) #归一化
        
        sample['case_name'] = image_file[-10:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample