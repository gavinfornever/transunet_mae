'''
Descripttion: Created by QiSen Ma
Author: QiSen Ma
Date: 2023-01-15 23:02:40
LastEditTime: 2023-01-19 23:31:14
'''
#所有数据集的数据加载类
import glob
import json
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import numpy as np
import random
import os
from pathlib import Path
from PIL import Image
import cv2
# from albumentations.pytorch import ToTensorV2
# import albumentations as A

seperable_indexes = json.load(open('./dataset/ISIC2018/data_split.json', 'r'))


class ISIC2018_DataLoader(Dataset):
    """ Dataloader ISIC2018

    Args:
        data_path: The path of dataset.
        list_path: The path of list file.
        split: Split type, "train" or "validation" or "test".
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='',list_path='./dataset/ISIC2018',
                 split='train', transform=None, threshold=0.3):
        self.data_path = data_path
        self.list_path = list_path
        self.split = split
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        if self.split == 'train':
            self.sample_list = open(os.path.join(self.list_path, 'train_pair.lst')).readlines()
        elif self.split == 'validation':
            self.sample_list = open(os.path.join(self.list_path, 'validation_pair.lst')).readlines()
        elif self.split == 'test':
            self.sample_list = open(os.path.join(self.list_path, 'test_pair.lst')).readlines()
        else:
            raise ValueError("Invalid split type!")

    def __len__(self):
        return len(self.sample_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file, mask_file = self.sample_list[index].split()
        #读取image
        image = cv2.imread(os.path.join(self.data_path, image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #读取mask
        mask = cv2.imread(os.path.join(self.data_path, mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])  
        sample['image'] = self.transform_toTensor(sample['image'])
        sample['edge'] = cv2.Canny(sample['mask'],30,100)/255. #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = sample['mask']/255. #归一化
        sample['case_name'] = image_file[-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample
    
    
class ISIC2018_kfold_DataLoader(Dataset):
    """ Dataloader ISIC2018

    Args:
        data_path: The path of dataset.
        split: Split type, "train" or "validation".
        fold:str, fold index of kfold.
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path, split, fold='0', transform=None, threshold=0.3):
        super(ISIC2018_kfold_DataLoader, self).__init__()
        self.split = split
        self.prefix_image = '/ISIC2018_Task1-2_Training_Input/'
        self.prefix_mask = '/ISIC2018_Task1_Training_GroundTruth/'
        # load images, mask
        self.paths_image = []
        self.paths_mask = []
        
        image_list = glob.glob(data_path+ self.prefix_image+'/*.jpg')
        indexes = [l[-11:-4] for l in image_list]

        valid_indexes = seperable_indexes[fold]
        train_indexes = list(filter(lambda x: x not in valid_indexes, indexes))
        
        print('Fold {}: train: {} valid: {}'.format(fold, len(train_indexes),len(valid_indexes)))

        indexes = train_indexes if split == 'train' else valid_indexes

        self.image_paths = [
            data_path + self.prefix_image + '/ISIC_{}.jpg'.format(_id) for _id in indexes
        ]
        self.mask_paths = [
            data_path + self.prefix_mask + '/ISIC_{}_segmentation.png'.format(_id) for _id in indexes
        ]
         
        print('Loaded {} frames'.format(len(self.image_paths)))
        self.num_samples = len(self.image_paths)
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])

        p = 0.5
        self.transf = A.Compose([
            A.GaussNoise(p=p),
            A.HorizontalFlip(p=p),
            A.VerticalFlip(p=p),
            A.ShiftScaleRotate(p=p),
            # A.RandomBrightnessContrast(p=p),
        ])

    def __getitem__(self, index):
        # print('image path:',self.image_paths[index])
        #通用流程、数据增强操作
        #读取image
        image = cv2.imread(self.image_paths[index],1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #读取mask
        mask = cv2.imread(self.mask_paths[index],0)#以灰度图的方式读取mask
        #数据增强操作
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        sample['image'] = self.transform_toTensor(sample['image'])
        sample['edge'] = cv2.Canny(sample['mask'],30,100)/255. #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = sample['mask']/255. #归一化
        sample['case_name'] = self.image_paths[index][-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample
        
    def __len__(self):
        return self.num_samples