'''
Descripttion: Created by QiSen Ma
Author: QiSen Ma
Date: 2023-01-15 23:02:40
LastEditTime: 2023-02-04 13:47:01
'''
#所有数据集的数据加载类
from torch.utils.data import Dataset, DataLoader
import torch
import torchvision.transforms as transforms
import numpy as np
import random
import os
from pathlib import Path
from PIL import Image
import cv2
# from albumentations.pytorch import ToTensorV2

# ===== normalize over the dataset 
def dataset_normalized(imgs):
    imgs_normalized = np.empty(imgs.shape)
    imgs_std = np.std(imgs)
    imgs_mean = np.mean(imgs)
    imgs_normalized = (imgs-imgs_mean)/imgs_std
    #对每一张图片分别进行正则化?
    for i in range(imgs.shape[0]):
        #注意这里正则化之后乘了255，这样使得正则化后的结果仍是取值在0-255之间的RGB图片的格式
        #并且还进行了取整操作，确保RGB图像的每一位都是整数，避免后续某些奇怪的bug的出现
        imgs_normalized[i] = (((imgs_normalized[i] - np.min(imgs_normalized[i])) / (np.max(imgs_normalized[i])-np.min(imgs_normalized[i])))*255).astype(np.uint8)  
    return imgs_normalized


class ISIC2018_DataLoader_NPY(Dataset):
    """ dataset class for Brats datasets
    """
    def __init__(self, data_path, split='train', transform=None):
        super(ISIC2018_DataLoader_NPY, self)
        self.split = split
        if self.split == "train":
          self.data   = np.load(data_path+'data_train.npy')
          self.mask   = np.load(data_path+'mask_train.npy')
        elif self.split == "test":
            self.data   = np.load(data_path+'data_test.npy')
            self.mask   = np.load(data_path+'mask_test.npy')
        elif self.split == "validation":
            self.data   = np.load(data_path+'data_val.npy')
            self.mask   = np.load(data_path+'mask_val.npy')          
          
          
        self.data   = dataset_normalized(self.data)
        self.transform = transform
        # self.mask   = np.expand_dims(self.mask, axis=3) #新增通道维度
        # self.mask   = self.mask /255.

    def __getitem__(self, indx):
        img = self.data[indx].astype(np.uint8)   
        seg = self.mask[indx].astype(np.uint8)   
                    
        if self.transform:
            # sample = self.transform(image=image,mask=mask)
            # img, seg = self.apply_augmentation(img, seg)
            transform_result= self.transform(image=img, mask=seg)
            img = transform_result['image']
            seg = transform_result['mask']
            
        #edge应该从img入手，而不是mask
        edge =  img.copy()
        edge = (cv2.Canny(edge.astype(np.uint8),30,100)/255.).astype(np.float32)   
        seg = (seg /255.).astype(np.float32)
        img = img.astype(np.float32)
        
        img = torch.tensor(img.copy())        
        seg = torch.tensor(seg.copy())
        edge = torch.tensor(edge.copy())
        
        img = img.permute( 2, 0, 1)# h,w,c -> c,h,w
        # seg = seg.permute( 2, 0, 1)# h,w,c -> c,h,w
        # edge = edge.permute( 2, 0, 1)# h,w,c -> c,h,w
        

        return {'image': img, 'mask' : seg, 'edge': edge}
               
    def apply_augmentation(self, img, seg):
        if random.random() < 0.5:
            img  = np.flip(img,  axis=1)
            seg  = np.flip(seg,  axis=1)
        return img, seg

    def __len__(self):
        return len(self.data)
    

class ISIC2017_DataLoader_NPY(Dataset):
    """ dataset class for Brats datasets
    """
    def __init__(self, data_path, split='train', transform=None):
        super(ISIC2017_DataLoader_NPY, self)
        self.split = split
        if self.split == "train":
          self.data   = np.load(data_path+'data_train_gw.npy')
          self.mask   = np.load(data_path+'mask_train_gw.npy')
        elif self.split == "test":
            self.data   = np.load(data_path+'data_test_gw.npy')
            self.mask   = np.load(data_path+'mask_test_gw.npy')
        elif self.split == "validation":
            self.data   = np.load(data_path+'data_val_gw.npy')
            self.mask   = np.load(data_path+'mask_val_gw.npy')          
          
          
        # self.data   = dataset_normalized(self.data)
        self.transform = transform
        # self.mask   = np.expand_dims(self.mask, axis=3) #新增通道维度
        # self.mask   = self.mask /255.

    def __getitem__(self, indx):
        img = self.data[indx].astype(np.uint8)   
        seg = self.mask[indx].astype(np.uint8)   
                    
        if self.transform:
            # sample = self.transform(image=image,mask=mask)
            # img, seg = self.apply_augmentation(img, seg)
            transform_result= self.transform(image=img, mask=seg)
            img = transform_result['image']
            seg = transform_result['mask']
            
        #edge应该从img入手，而不是mask
        edge =  img.copy()
        edge = (cv2.Canny(edge.astype(np.uint8),30,100)/255.).astype(np.float32)   
        seg = (seg /255.).astype(np.float32)
        img = img.astype(np.float32)
        
        img = torch.tensor(img.copy())        
        seg = torch.tensor(seg.copy())
        edge = torch.tensor(edge.copy())
        
        img = img.permute( 2, 0, 1)# h,w,c -> c,h,w
        # seg = seg.permute( 2, 0, 1)# h,w,c -> c,h,w
        # edge = edge.permute( 2, 0, 1)# h,w,c -> c,h,w
        

        return {'image': img, 'mask' : seg, 'edge': edge}
               
    def apply_augmentation(self, img, seg):
        if random.random() < 0.5:
            img  = np.flip(img,  axis=1)
            seg  = np.flip(seg,  axis=1)
        return img, seg

    def __len__(self):
        return len(self.data)

class PH2_DataLoader_NPY(Dataset):
    """ dataset class for Brats datasets
    """
    def __init__(self, data_path, split='train', transform=None):
        super(PH2_DataLoader_NPY, self)
        self.split = split
        if self.split == "train":
          self.data   = np.load(data_path+'data_train_gw.npy')
          self.mask   = np.load(data_path+'mask_train_gw.npy')
        elif self.split == "test":
            self.data   = np.load(data_path+'data_test_gw.npy')
            self.mask   = np.load(data_path+'mask_test_gw.npy')
        elif self.split == "validation":
            self.data   = np.load(data_path+'data_val_gw.npy')
            self.mask   = np.load(data_path+'mask_val_gw.npy')          
          
          
        # self.data   = dataset_normalized(self.data)
        self.transform = transform
        # self.mask   = np.expand_dims(self.mask, axis=3) #新增通道维度
        # self.mask   = self.mask /255.

    def __getitem__(self, indx):
        img = self.data[indx].astype(np.uint8)   
        seg = self.mask[indx].astype(np.uint8)   
                    
        if self.transform:
            # sample = self.transform(image=image,mask=mask)
            # img, seg = self.apply_augmentation(img, seg)
            transform_result= self.transform(image=img, mask=seg)
            img = transform_result['image']
            seg = transform_result['mask']
            
        #edge应该从img入手，而不是mask
        edge =  img.copy()
        edge = (cv2.Canny(edge.astype(np.uint8),30,100)/255.).astype(np.float32)   
        seg = (seg /255.).astype(np.float32)
        img = img.astype(np.float32)
        
        img = torch.tensor(img.copy())        
        seg = torch.tensor(seg.copy())
        edge = torch.tensor(edge.copy())
        
        img = img.permute( 2, 0, 1)# h,w,c -> c,h,w
        # seg = seg.permute( 2, 0, 1)# h,w,c -> c,h,w
        # edge = edge.permute( 2, 0, 1)# h,w,c -> c,h,w
        

        return {'image': img, 'mask' : seg, 'edge': edge}
               
    def apply_augmentation(self, img, seg):
        if random.random() < 0.5:
            img  = np.flip(img,  axis=1)
            seg  = np.flip(seg,  axis=1)
        return img, seg

    def __len__(self):
        return len(self.data)




class ISIC2018_DataLoader(Dataset):
    """ Dataloader ISIC2018

    Args:
        data_path: The path of dataset.
        list_path: The path of list file.
        split: Split type, "train" or "validation" or "test".
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='',list_path='./dataset/ISIC2018',
                 split='train', transform=None, threshold=0.3):
        self.data_path = data_path
        self.list_path = list_path
        self.split = split
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        if self.split == 'train':
            self.sample_list = open(os.path.join(self.list_path, 'train_pair.lst')).readlines()
        elif self.split == 'validation':
            self.sample_list = open(os.path.join(self.list_path, 'validation_pair.lst')).readlines()
        elif self.split == 'test':
            self.sample_list = open(os.path.join(self.list_path, 'test_pair.lst')).readlines()
        else:
            raise ValueError("Invalid split type!")

    def __len__(self):
        return len(self.sample_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file, mask_file = self.sample_list[index].split()
        # print('image_file:',image_file)
        
        #读取image
        image = cv2.imread(os.path.join(self.data_path, image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #读取mask
        mask = cv2.imread(os.path.join(self.data_path, mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])
        
        sample['edge'] =    (cv2.Canny(sample['image'],30,100)/255.).astype(np.float32) #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['image'] = self.transform_toTensor(sample['image'])
        sample['mask'] = sample['mask']/255. #归一化
        sample['case_name'] = image_file[-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample
    

class ISIC2018_DataLoader_NPY_gw(Dataset):
    """ dataset class for Brats datasets
    """
    def __init__(self, data_path, split='train', transform=None):
        super(ISIC2018_DataLoader_NPY_gw, self)
        self.split = split
        if self.split == "train":
          self.data   = np.load(data_path+'data_train.npy')
          self.mask   = np.load(data_path+'mask_train.npy')
        elif self.split == "test":
            self.data   = np.load(data_path+'data_test.npy')
            self.mask   = np.load(data_path+'mask_test.npy')
        elif self.split == "validation":
            self.data   = np.load(data_path+'data_val.npy')
            self.mask   = np.load(data_path+'mask_val.npy')          
          
          
        self.data   = dataset_normalized(self.data)
        self.transform = transform
        # self.mask   = np.expand_dims(self.mask, axis=3) #新增通道维度
        # self.mask   = self.mask /255.

    def __getitem__(self, indx):
        img = self.data[indx].astype(np.uint8)   
        seg = self.mask[indx].astype(np.uint8)   
                    
        if self.transform:
            if len(img.shape) != 3 or img.shape[2] != 3:
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            transform_result = self.transform(image=img, mask=seg)
            img = transform_result['image']
            seg = transform_result['mask']
        
        img = cv2.resize(img, (224, 224))
        seg = cv2.resize(seg, (224, 224))

        img = np.expand_dims(img, axis=2)
        seg = np.expand_dims(seg, axis=2)
        
        #edge应该从img入手，而不是mask
        edge =  img.copy()
        edge = (cv2.Canny(edge.astype(np.uint8),30,100)/255.).astype(np.float32)   
        seg = (seg /255.).astype(np.float32)
        img = img.astype(np.float32)
        
        img = torch.tensor(img.copy())        
        seg = torch.tensor(seg.copy())
        edge = torch.tensor(edge.copy())
        
        img = img.permute( 2, 0, 1)# h,w,c -> c,h,w
        # seg = seg.permute( 2, 0, 1)# h,w,c -> c,h,w
        # edge = edge.permute( 2, 0, 1)# h,w,c -> c,h,w
        

        return {'image': img, 'mask' : seg, 'edge': edge}
               
    def apply_augmentation(self, img, seg):
        if random.random() < 0.5:
            img  = np.flip(img,  axis=1)
            seg  = np.flip(seg,  axis=1)
        return img, seg

    def __len__(self):
        return len(self.data)