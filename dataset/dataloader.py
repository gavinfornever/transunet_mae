'''
Descripttion: Created by QiSen Ma
Author: QiSen Ma
Date: 2023-01-15 23:02:40
LastEditTime: 2023-02-04 13:47:01
'''
#所有数据集的数据加载类
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import numpy as np
import random
import os
from pathlib import Path
from PIL import Image
import cv2
# from albumentations.pytorch import ToTensorV2

class ISIC2018_DataLoader(Dataset):
    """ Dataloader ISIC2018

    Args:
        data_path: The path of dataset.
        list_path: The path of list file.
        split: Split type, "train" or "validation" or "test".
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='',list_path='./dataset/ISIC2018',
                 split='train', transform=None, threshold=0.3):
        self.data_path = data_path
        self.list_path = list_path
        self.split = split
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        if self.split == 'train':
            self.sample_list = open(os.path.join(self.list_path, 'train_pair.lst')).readlines()
        elif self.split == 'validation':
            self.sample_list = open(os.path.join(self.list_path, 'validation_pair.lst')).readlines()
        elif self.split == 'test':
            self.sample_list = open(os.path.join(self.list_path, 'test_pair.lst')).readlines()
        else:
            raise ValueError("Invalid split type!")

    def __len__(self):
        return len(self.sample_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file, mask_file = self.sample_list[index].split()
        # print('image_file:',image_file)
        
        #读取image
        image = cv2.imread(os.path.join(self.data_path, image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #读取mask
        mask = cv2.imread(os.path.join(self.data_path, mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])
        sample['image'] = self.transform_toTensor(sample['image'])
        sample['edge'] =    (cv2.Canny(sample['mask'],30,100)/255.).astype(np.float32) #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = (sample['mask']/255.).astype(np.float32) #归一化
        sample['case_name'] = image_file[-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample
    
class ISIC2017_DataLoader(Dataset):
    """ Dataloader ISIC2018

    Args:
        data_path: The path of dataset.
        list_path: The path of list file.
        split: Split type, "train" or "validation" or "test".
        transform: A series of data augmentation operations.
        threshold: Threshold for edge map's ground truth.
    """
    def __init__(self, data_path='',list_path='./dataset/ISIC2018',
                 split='train', transform=None, threshold=0.3):
        self.data_path = data_path
        self.list_path = list_path
        self.split = split
        self.threshold = threshold * 256
        self.transform = transform #之所以不再数据集里处理是因为考虑到对应训练集、验证集和测试集可能有不同的数据增强处理
        self.transform_toTensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        if self.split == 'train':
            self.sample_list = open(os.path.join(self.list_path, 'train_pair.lst')).readlines()
        elif self.split == 'validation':
            self.sample_list = open(os.path.join(self.list_path, 'validation_pair.lst')).readlines()
        elif self.split == 'test':
            self.sample_list = open(os.path.join(self.list_path, 'test_pair.lst')).readlines()
        else:
            raise ValueError("Invalid split type!")

    def __len__(self):
        return len(self.sample_list)

    def __getitem__(self, index):
        #通用流程、数据增强操作
        image_file, mask_file = self.sample_list[index].split()
        # print('image_file:',image_file)
        
        #读取image
        image = cv2.imread(os.path.join(self.data_path, image_file),1) #以BGR方式读取图像
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #读取mask
        mask = cv2.imread(os.path.join(self.data_path, mask_file),0)#以灰度图的方式读取mask
        #数据增强操作
        # sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(image=image,mask=mask)
        # sample['image'] = ToTensorV2(sample['image'])
        sample['image'] = self.transform_toTensor(sample['image'])
        sample['edge'] =    (cv2.Canny(sample['mask'],30,100)/255.).astype(np.float32) #利用cv2.Canny得到edge信息并进行归一化，标签为0，1
        sample['mask'] = (sample['mask']/255.).astype(np.float32) #归一化
        sample['case_name'] = image_file[-16:-4]
        # sample = {'image': image, 'mask': mask, 'edge:' edge，'case_name':case_name}
        return sample