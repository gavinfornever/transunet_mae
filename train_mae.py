import torch.backends.cudnn as cudnn
import yaml
import sys
import time
import argparse
import logging
import os
import random
import numpy as np
import torch
import albumentations as A
import cv2
import torch.backends.cudnn as cudnn
import torch.nn as nn
from torch.nn.modules.loss import CrossEntropyLoss

sys.path.append("../../") 
from dataset.dataloader_npy import ISIC2018_DataLoader_NPY,ISIC2017_DataLoader_NPY,PH2_DataLoader_NPY
from torch.utils.data import DataLoader
from networks.vit_seg_modeling import VisionTransformer as ViT_seg
from networks.vit_seg_modeling import CONFIGS as CONFIGS_ViT_seg
import torch.nn.functional as F
from losses import BCELoss, DiceLoss, cross_entropy_loss_RCF_Multi_Scale, cross_entropy_loss_RCF
import torch.optim as optim
from tqdm import tqdm
from utils import AverageMeter
import copy
from tensorboardX import SummaryWriter

#固定种子
def set_seed(seed_value=42):
    """Set seed for reproducibility."""
    random.seed(seed_value)
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)
    torch.cuda.manual_seed_all(seed_value)
SEED = 42
set_seed(SEED)
# Ensure that all operations are deterministic on GPU (if used) for reproducibility
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
#计算模型参数量的函数
def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)
os.environ['CUDA_LAUNCH_BLOCKING'] = '1' # 下面老是报错 shape 不一致
train_config = yaml.load(open('./configs.yml'), Loader=yaml.FullLoader)

torch.cuda.set_device(train_config['num_cuda'])

if __name__ == "__main__":
    torch.backends.cudnn.enabled = False
    data_transform = {
        "train": A.Compose([
                # A.Resize(height=int(swin_config.DATA.IMG_SIZE * 1.143), width=int(swin_config.DATA.IMG_SIZE * 1.143), p=1.0),
                # # # A.CenterCrop(swin_config.DATA.IMG_SIZE,swin_config.DATA.IMG_SIZE),
                # A.RandomResizedCrop(height=swin_config.DATA.IMG_SIZE,width=swin_config.DATA.IMG_SIZE,scale=(0.8, 1.0), ratio=(0.8, 1.25),interpolation=cv2.INTER_LINEAR,p=1.0),
                A.Resize(height=224, width=224, p=1.0),
                A.ShiftScaleRotate(shift_limit=0, scale_limit=0, rotate_limit=15, p=0.7),
                A.Flip(always_apply=False, p=0.7),
                A.HueSaturationValue(hue_shift_limit=5, sat_shift_limit= 30, val_shift_limit=20, p=0.7),
                A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=0.7),
                A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0, always_apply=False, p=1.0),
            ]),
        "validation": A.Compose([
                # A.Resize(height=int(swin_config.DATA.IMG_SIZE * 1.143), width=int(swin_config.DATA.IMG_SIZE * 1.143), p=1.0),
                # A.CenterCrop(swin_config.DATA.IMG_SIZE,swin_config.DATA.IMG_SIZE),
                A.Resize(height=224, width=224, p=1.0),
                A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0, always_apply=False, p=1.0),
            ])
    }
    train_dataset = ISIC2017_DataLoader_NPY(data_path=train_config['data_path'],split='train',transform=data_transform['train'])
    trainloader = DataLoader(train_dataset, batch_size=train_config['batch_size_tr'], shuffle=True, num_workers=6)
    valid_dataset = ISIC2017_DataLoader_NPY(data_path=train_config['data_path'],split='validation',transform=data_transform['validation'])
    validloader = DataLoader(valid_dataset, batch_size=train_config['batch_size_va'], shuffle=True, num_workers=6)
    config_vit = CONFIGS_ViT_seg[train_config['vit_name']]
    config_vit.n_classes = train_config['num_classes']
    config_vit.n_skip = train_config['n_skip']
    if train_config['vit_name'].find('R50') != -1:
        config_vit.patches.grid = (int(train_config['img_size'] / train_config['vit_patches_size']), int(train_config['img_size'] / train_config['vit_patches_size']))
    model = ViT_seg(config_vit, img_size=train_config['img_size'], reform_mae=train_config['reform_mae'],
                    num_classes=config_vit.n_classes).cuda()
    # 可能从pretrained_path或resnet_pretrained_path获取权重
    # if config_vit.resnet_pretrained_path != None:
    #     print("load from: ",config_vit.resnet_pretrained_path)
    #     model.load_from(weights=np.load(config_vit.resnet_pretrained_path))
    print("load from: ",config_vit.pretrained_path)
    model.load_from(weights=np.load(config_vit.pretrained_path))
    if train_config['pretrained'] !='':
        model.load_state_dict(torch.load(train_config['pretrained'])['model_weights'], strict=True)
    ce_loss = BCELoss()
    dice_loss = DiceLoss()
    optimizer1 = optim.SGD(model.parameters(), lr=float(train_config['lr_seg']), weight_decay=0.001)
    optimizer2 = optim.SGD(model.parameters(), lr=float(train_config['lr_mae']), weight_decay=0.001)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer1, 'min', factor = 0.5, patience = 10)
    # scheduler2 = optim.lr_scheduler.ReduceLROnPlateau(optimizer2, 'min', factor = 0.5, patience = 10)
    experiment_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(time.time())))
    iter_num = 0
    max_epoch = train_config['epochs']
    max_iterations = train_config['epochs'] * len(trainloader)  # max_epoch = max_iterations // len(trainloader) + 1
    snapshot_path = './save_models'
    best_performance = 0.0
    best_val_loss  = np.inf
    # 平均数存储与计算器，在展示时用
    losses_ce = AverageMeter();losses_dice = AverageMeter()
    losses_reform_seg = AverageMeter()
    losses_seg = AverageMeter();losses_edge = AverageMeter()
    losses_boundary = AverageMeter()
    losses_total = AverageMeter()

    save_dir = './save_models/'+ train_config['experiment_name']
    if not os.path.isdir('./save_models/'+ train_config['experiment_name']):
        os.makedirs(save_dir)
    writer = SummaryWriter(train_config['experiment_name'] + '')
    withmae = train_config['withmae']
    reform_seg = train_config['reform_seg']
    # loss_seg
    def reform_segloss(pred, gt):
        def patchify_single(imgs):
            """
            imgs: (N, 1, H, W)  # Assuming single-channel images
            x: (N, L, patch_size**2)
            """
            p = train_config['vit_patches_size']
            assert imgs.shape[2] == imgs.shape[3] and imgs.shape[2] % p == 0
            h = w = imgs.shape[2] // p
            x = imgs.reshape(shape=(imgs.shape[0], 1, h, p, w, p))  # Use 1 channel instead of 3
            x = torch.einsum('nchwpl->nhwpcl', x)  # Adjust the einsum string for single channel
            x = x.reshape(shape=(imgs.shape[0], h * w, p**2))  # Remove the *3 for single channel
            return x
        # 把图片转为[N, L, p*p] N,196,256
        pred = patchify_single(pred)
        gt = gt.unsqueeze(1)
        gt = patchify_single(gt)
        # 获取N,L
        N, L = pred.shape[0], pred.shape[1]
        # 计算出[16, 224, 224]的每个pixel的损失
        # loss_entropy = nn.BCEWithLogitsLoss()(pred,gt)
        loss = (pred - gt) ** 2
        loss = loss.mean(dim=-1)
        sum_loss = torch.sum(loss, dim=1, keepdim=True)
        # print(sum_loss.sum()) # scale前的loss
        beta = 0.5
        scaled_loss = loss / beta
        softmaxed_loss = F.softmax(scaled_loss, -1)
        loss_weighted = sum_loss * softmaxed_loss
        sum_pixel = N * L
        loss = loss_weighted.sum() / sum_pixel
        # print(loss) # scale后的loss
        return loss


    for epoch_num in tqdm(range(max_epoch), ncols=50):
        model.train()
        for i_batch, sampled_batch in enumerate(trainloader):
            image_batch, label_batch = sampled_batch['image'], sampled_batch['mask']
            image_batch, label_batch = image_batch.cuda(), label_batch.cuda()

            outputs_feature = model(image_batch)
            # print(outputs_feature.shape)#torch.Size([24, 1, 224, 224])
            loss_dice = dice_loss(outputs_feature, label_batch) #对于每个类别进行预测
            loss_ce = ce_loss(outputs_feature, label_batch)
            loss_seg = train_config['lamda_dice'] * loss_dice + train_config['lamda_ce'] * loss_ce 
            if reform_seg:
                loss_reform_seg = reform_segloss(outputs_feature, label_batch)
                loss_seg += train_config['lamda_reform_seg'] * loss_reform_seg
            optimizer1.zero_grad()
            loss_seg.backward()
            optimizer1.step()

            if withmae:
                loss2, pred, mask = model(image_batch, mode='mae')
                optimizer2.zero_grad()
                loss2.backward()
                optimizer2.step()
            
            iter_num = iter_num + 1

            bsz = image_batch.shape[0]
            losses_ce.update(loss_ce, bsz)
            losses_dice.update(loss_dice, bsz)
            if reform_seg:
                losses_reform_seg.update(loss_reform_seg, bsz)
            losses_seg.update(loss_seg, bsz)
            if i_batch % int(float(train_config['progress_p']) * len(trainloader))==0:
                print('\n----------------------------------------------------------------------')
                print(
                    f"Train: [{epoch_num}][{i_batch + 1}/{len(trainloader)}]\t\t"
                    f"losses_total: {losses_total.val:.3f} ({losses_total.avg:.3f})\n"
                    f"loss_ce: {losses_ce.val:.3f} ({losses_ce.avg:.3f})\t\t"
                    f"losses_dice: {losses_dice.val:.3f} ({losses_dice.avg:.3f})\n"
                    f"losses_seg: {losses_seg.val:.3f} ({losses_seg.avg:.3f})\t"
                    
                    # f"loss_boundary: {losses_boundary.val:.3f} ({losses_boundary.avg:.3f})"
                )
                if reform_seg:
                    print(f"losses_reform_seg: {losses_reform_seg.val:.3f} ({losses_reform_seg.avg:.3f})\t")
                if withmae:
                    print(f"losses_mae {loss2.item():.3f}\t")
                print('----------------------------------------------------------------------')
                sys.stdout.flush()  #清空缓冲区，立即输出打印结果
            writer.add_scalar('train/loss_ce', loss_ce, iter_num)
            writer.add_scalar('train/losses_dice', losses_dice.avg, iter_num)
            writer.add_scalar('train/losses_seg', losses_seg.avg, iter_num)
            if reform_seg:
                writer.add_scalar('train/losses_reform_seg', losses_reform_seg.avg, iter_num)
            if withmae:
                writer.add_scalar('train/losses_mae', loss2, iter_num)
            # record train effect image
            if iter_num % 20 == 0:
                image = image_batch[0, 0:1, :, :]
                image = (image - image.min()) / (image.max() - image.min())
                
                # outputs = torch.argmax(torch.softmax(outputs_feature, dim=1), dim=1, keepdim=True)
                outputs = outputs_feature
                # print("outputs[1, ...].shape",outputs[1, ...].shape)
                # print("outputs_boundary[1, ...].shape",outputs_boundary[1, ...].shape)
                # print("label_batch[1, ...].unsqueeze(0)",label_batch[1, ...].unsqueeze(0).shape)
                # print("output_edge[1,4,...]",output_edge[4].shape)
                labs = label_batch[0, ...].unsqueeze(0) * 50

 
        ## Validation phase
        total_eva_mae_loss = 0
        with torch.no_grad():
            val_loss = 0
            val_loss_ce = 0
            val_loss_dice = 0
            val_loss_reform_seg = 0
            model.eval()
            for itter, sampled_batch in enumerate(validloader):
                image_batch, label_batch = sampled_batch['image'], sampled_batch['mask']
                image_batch, label_batch = image_batch.cuda(), label_batch.cuda()
                outputs_feature= model(image_batch)
                # 验证集仅计算l_seg
                loss_ce = ce_loss(outputs_feature, label_batch)
                loss_dice = dice_loss(outputs_feature, label_batch)
                loss_seg = train_config['lamda_dice'] * loss_dice + train_config['lamda_ce'] * loss_ce 
                if reform_seg:
                    loss_reform_seg = reform_segloss(outputs_feature, label_batch)
                    loss_seg += train_config['lamda_reform_seg'] * loss_reform_seg
                
                if withmae:
                    loss2, pred, mask = model(image_batch, mode='mae')
                    total_eva_mae_loss = total_eva_mae_loss + loss2.item()


                val_loss_ce += loss_ce.item()
                val_loss_dice += loss_dice.item()
                if reform_seg:
                    val_loss_reform_seg += loss_reform_seg.item()
                val_loss += loss_seg.item()
                
            writer.add_scalar('validation/loss_ce', loss_ce, epoch_num)
            writer.add_scalar('validation/losses_dice', losses_dice.avg, epoch_num)
            if reform_seg:
                writer.add_scalar('validation/losses_reform_seg', losses_reform_seg.avg, epoch_num)
            writer.add_scalar('validation/losses_seg', losses_seg.avg, epoch_num)
            if withmae:
                writer.add_scalar('validation/losses_mae', total_eva_mae_loss, epoch_num)
            print(f' validation on epoch>> {epoch_num} semantic loss>> {(abs(val_loss/(itter+1)))} '
                  f'ce loss>> {(abs(val_loss_ce/(itter+1)))}  dice loss>> {(abs(val_loss_dice/(itter+1)))} ')
            if reform_seg:
                print(f'reform_seg loss>> {(abs(val_loss_reform_seg/(itter+1)))} ')
            if withmae:
                print("本次训练后验证集的Mae_loss：{}".format(total_eva_mae_loss))
            mean_val_loss = (val_loss/(itter+1))
            # Check the performance and save the model
            # if (mean_val_loss) < best_val_loss and epoch_num > int(3 * max_epoch / 4):
            if (mean_val_loss) < best_val_loss and epoch_num > int(max_epoch / 2):
                print('New best loss, saving...')
                best_val_loss = copy.deepcopy(mean_val_loss)
                state = copy.deepcopy({'model_weights': model.state_dict(), 'val_loss': best_val_loss})
                torch.save(state, save_dir +'/best_epochnum_'+ str(epoch_num) +'_weights_ISIC17.model')

        
                
        # save model per interval
        save_interval = train_config['save_interval']
        if epoch_num > int(max_epoch / 2) and (epoch_num + 1) % save_interval == 0:
            state = copy.deepcopy({'model_weights': model.state_dict(), 'val_loss': best_val_loss})
            torch.save(state, save_dir +'/interval_save_'+ str(epoch_num) +'_weights_ISIC17.model')    
                
        scheduler.step(mean_val_loss)


  