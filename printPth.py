import torch
import numpy as np
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# 打印权重文件内部的参数
# weights_files = "./weights/mae_pretrain_vit_large.pth"
# weights = torch.load(weights_files, map_location=device)['model']
# # weights = weights["model"]

# for k, v in weights.items(): 
#     print(k,"---",v.shape)

print("-------------------------------------------------------")

# pth
# 打印导入权重后的模型
# model = Naive(reform=False,beta=0.5,
#                 embed_dim=1024,num_heads=16,depth=24,
#                 decoder_embed_dim=1024,decoder_depth=8,decoder_num_heads=16)
# model = model.cuda()
# weights_files = "./ckpts/Naive_mae_Large_mae_weight/semae_1.pth"
# weights_files = "/home/shiyanshi/wg/setr-main/weights/pretrained_mae_ViT-L_1000_1024decoder.pth"
# # weights_files = "../mae/weights/mae_pretrain_vit_large.pth"
# model = torch.load(weights_files)
# with open("/home/shiyanshi/wg/setr-main/weights/pretrained_mae_ViT-L_1000_1024decoder_files.txt",'w')as f:
#     for k, v in model.items():
#         print(k," - ",v.shape, file=f)

# print("-------------------------------------------------------")
# # npz
npz = np.load("/mnt/share/models/tzy/wgao/TransUnet_ISIC18/model/vit_checkpoint/imagenet21k/ViT-L_16.npz")
with open("/mnt/share/models/tzy/wgao/TransUnet_ISIC18/model/vit_checkpoint/imagenet21k/ViT-L_16.txt",'w')as f:
    for x in npz.files:
        v = npz[str(x)]
        print(x,' - ',v.shape, file=f)
