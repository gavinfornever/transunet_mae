import os
import subprocess
import pandas as pd
import yaml

model_names = []
f1_scores = []
sensitivities = []
specificities = []
accuracies = []
ious = []

# 加载config.yml文件
config_file = "configs.yml"
with open(config_file, "r") as f:
    configs = yaml.safe_load(f)

# 模型名字的前缀
model_name_prefix = "TransUnet_PH2"
# 循环遍历需要更改的save_model的值
for i in range(55, 65):
    # 更新config中的save_model值
    save_model = f"/home/pingyi/mqs/models/LCAUnet/model_comparison/TransUnet/save_models/Trans_PH2_2/interval_save_{i}_weights_ISIC17.model"
    configs["saved_model"] = save_model

    # 将更新后的config保存到临时文件中
    temp_config_file = "temp_config.yml"
    with open(temp_config_file, "w") as f:
        yaml.safe_dump(configs, f)
    # os.environ["MODEL_CONFIG"] = temp_config_file
    # 运行 evaluate.py 并获取输出结果
    command = f"python evaluate.py"
    output = subprocess.check_output(command, shell=True).decode()
    # print(output)

    # 解析输出结果，获取评估指标
    lines = output.strip().split("\n")
    f1_score = float(lines[4].split(":")[1].strip())
    sensitivity = float(lines[7].split(":")[1].strip())
    specificity = float(lines[8].split(":")[1].strip())
    accuracy = float(lines[9].split(":")[1].strip())
    iou = float(lines[10].split(":")[1].strip())

    # 将结果保存到列表中
    model_names.append(f"{model_name_prefix}")
    f1_scores.append(f1_score)
    sensitivities.append(sensitivity)
    specificities.append(specificity)
    accuracies.append(accuracy)
    ious.append(iou)

# 创建一个字典，用于构建DataFrame
data = {
    "Model Name": model_names,
    "F1 Score": f1_scores,
    "Sensitivity": sensitivities,
    "Specificity": specificities,
    "Accuracy": accuracies,
    "IoU": ious
}

# 创建DataFrame
df = pd.DataFrame(data)

# 保存DataFrame到Excel文件
df.to_excel("Trans_PH2_2.xlsx", index=False)

# 删除临时的config文件
os.remove(temp_config_file)
